# People-Statst-Fanny-Goubert

Évaluation module UI/UX Design My Digital School

Bonjour Monsieur, ci-joint mon travail d'évaluation pour le module UI/UX Design. Vous trouverez dans ce repo mes maquettes fonctionnelles et graphiques en format pdf ainsi qu'au format .bmpr pour la maquette fonctionnelle (faite avec Balsamiq mockup) et un fichier People Stats.xd qui comporte ma maquette graphique.
J'ai été un peu plus loin dans ce travail et j'y ai ajouté des transitions que vous pourrez voir avec ce lien lien partage adobe XD : https://xd.adobe.com/view/676a85bc-78d8-46bf-b927-50aee8759d31-3239/

Bien cordialement,

Fanny GOUBERT
B3 Dev 
MyDigitalSchool campus de Melun
